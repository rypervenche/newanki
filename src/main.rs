use rusqlite::{params, Connection, Result};
use std::collections::HashMap;

const DB_FILE: &str = "/tmp/newanki.db";

#[derive(Debug)]
struct Card {
    id: usize,
    deck_name: String,
    mandarin: String,
    english: String,
}

fn create_db(conn: &mut Connection) -> Result<()> {
    let tx = conn.transaction()?;
    tx.execute(
        "CREATE TABLE IF NOT EXISTS notes (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            deck_id INTEGER NOT NULL,
            mandarin TEXT NOT NULL,
            english TEXT NOT NULL,
            UNIQUE(deck_id, mandarin),
            FOREIGN KEY(deck_id) REFERENCES decks(id)
        )",
        [],
    )?;
    tx.execute(
        "CREATE TABLE IF NOT EXISTS decks (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL UNIQUE
        )",
        [],
    )?;
    tx.commit()
}

fn get_deck_id(conn: &Connection, deck_name: &str) -> Result<usize> {
    conn.execute(
        "INSERT OR IGNORE INTO decks (name) VALUES (?1)",
        params![deck_name],
    )?;

    let mut stmt = conn.prepare("SELECT id FROM decks WHERE name = ?")?;
    let deck_id: usize = stmt.query_row(params![deck_name], |row| row.get(0))?;
    Ok(deck_id)
}

fn update_db(conn: &mut Connection, deck_id: usize, mandarin: &str, english: &str) -> Result<()> {
    conn.execute(
        "INSERT INTO notes (deck_id, mandarin, english) VALUES (?1, ?2, ?3)",
        params![deck_id, mandarin, english],
    )?;
    Ok(())
}

fn get_cards(conn: &Connection, deck_id: usize) -> Result<HashMap<String, Card>> {
    let mut cards = HashMap::new();
    let mut stmt = conn.prepare(&format!(
        "SELECT notes.id, decks.name, mandarin, english FROM notes INNER JOIN decks ON notes.deck_id = decks.id WHERE deck_id = {}",
        deck_id
    ))?;
    let card_iter = stmt.query_map([], |row| {
        Ok(Card {
            id: row.get(0)?,
            deck_name: row.get(1)?,
            mandarin: row.get(2)?,
            english: row.get(3)?,
        })
    })?;
    for card in card_iter {
        let card = card.unwrap();
        cards.insert(card.mandarin.clone(), card);
    }
    Ok(cards)
}

fn main() -> Result<()> {
    let mut conn = Connection::open(DB_FILE)?;
    conn.execute("PRAGMA foreign_keys = ON", [])?;
    create_db(&mut conn)?;

    let chosen_deck = "test";
    let deck_id = get_deck_id(&conn, chosen_deck)?;

    update_db(&mut conn, deck_id, "虛空", "void")?;
    update_db(&mut conn, deck_id, "阿樂", "Alex")?;
    update_db(&mut conn, deck_id, "好煩", "is annoying")?;

    let cards = get_cards(&conn, deck_id)?;

    for card in cards {
        println!("{:#?}", card);
    }

    Ok(())
}
